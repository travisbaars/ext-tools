from .get import get_extension


# run program cli
if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("filename")

    args = parser.parse_args()

    action = get_extension(args.filename)

    print(action)
