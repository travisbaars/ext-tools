from .valid_ext import ext_dict


class _GetExtension:
    """
    This class is used to get the file extension from agiven string.
    Handling files is not currently supported.
    """

    def _get_extension(self, path_to_file):
        """"""
        normalized = self._normalize(path_to_file)
        split = normalized.split(".")
        compare = self._compare(split)

        if len(compare) > 1:
            extensions = ".".join(x for x in compare)

        elif len(compare) == 1:
            extensions = compare[0]

        else:
            raise ValueError("length of list can't be 0")

        return extensions

    def _normalize(self, path_to_file):
        """Used to normalize file names"""
        lower = path_to_file.lower()
        remove_space = lower.replace(" ", "")
        normal = str(remove_space)

        return normal

    def _compare(self, dot_split: list):
        """Checks the list of split items and checks each one against the valid extension dictionary"""
        extension = []
        for item in dot_split:
            if any(item == ext for ext in ext_dict):
                # ext = i
                # print("valid")
                # return ext
                extension.append(item)
            else:
                pass

        if not extension:
            raise ValueError("string does not contain a valid extension")
        return extension


_ext = _GetExtension()


# Use this public funcion to access the _Get_Extension class
def get_extension(path_to_file):
    return _ext._get_extension(path_to_file)
