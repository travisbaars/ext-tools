import pytest


@pytest.fixture
def single_ext_example():
    return "simple-0.0.1-py3-none-any.whl"


@pytest.fixture
def multi_ext_example():
    return "simple-0.0.1.tar.gz"


@pytest.fixture
def missing_ext_example():
    return "simple"
