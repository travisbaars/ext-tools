import pytest

from ext_tools.get import get_extension


def test_get_extension_single(single_ext_example):
    assert get_extension(single_ext_example) == "whl"


def test_get_extension_multi(multi_ext_example):
    assert get_extension(multi_ext_example) == "tar.gz"


def test_get_extension_missing(missing_ext_example):
    with pytest.raises(ValueError) as error:
        get_extension(missing_ext_example)
    assert str(error.value) == "string does not contain a valid extension"
