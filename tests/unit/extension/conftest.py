import pytest

from ext_tools.get import _GetExtension


@pytest.fixture
def init_extension():
    init = _GetExtension()

    return init


@pytest.fixture
def input_list():
    x = ["simple", ".", "txt"]

    return x
