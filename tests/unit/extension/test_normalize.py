def test_normalize(init_extension):
    assert init_extension._normalize("xXx") == "xxx"
    assert (
        init_extension._normalize("simple-0.0.1-py3-none-any.whl")
        == "simple-0.0.1-py3-none-any.whl"
    )
